const cproc = require('child_process');
const fs = require('fs-extra');
const https = require('follow-redirects').https;
const os = require('os');
const path = require('path');
const pkg = require('../package.json');

const desktopEntry = function desktopEntry () {

    return `[Desktop Entry]
Type=Application
Name=${pkg.build.linux.displayName}
Comment=${pkg.description}
Exec=${pkg.name}
Icon=${pkg.build.linux.icon}
Categories=${pkg.build.linux.categories}
Copyright=${pkg.build.linux.copyright}
`
};

const copyRecursiveSync = function copyRecursiveSync (src, dest) {
    fs.copySync(src, dest);

    fs.readdirSync(src)
        .map((name) => name)
        .filter((dir) => fs.lstatSync(path.join(src, dir)).isDirectory())
        .forEach((dir) => {
            copyRecursiveSync(path.join(src, dir), path.join(dest, dir));
        });
};

const fetch = function fetch (url, dest) {

    return new Promise((resolve, reject) => {
        const [, , urlhost, ...urlpath] = url.split('/');
        const file = path.basename(url);

        const request = https.request(
            {
                'host': urlhost,
                'path': `/${urlpath.join('/')}`
            },
            (response) => {
                if (response.statusCode !== 200) {
                    const err = new Error(`File ${file} could not be retrieved`);
                    err.status = response.statusCode;
                    reject(err);
                }

                let chunks = [];

                response.setEncoding('binary');

                response.on('data', (chunk) => {
                    chunks.push(Buffer.from(chunk, 'binary'));
                })
                    .on('end', () => {
                        const fileStream = fs.createWriteStream(path.join(dest, file));
                        const fileBuffer = Buffer.concat(chunks);

                        fileStream.write(fileBuffer, 'binary');

                        fileStream.on('finish', () => {
                            fileStream.close();
                            resolve();
                        });

                        response.pipe(fileStream);
                    })
            }
        );

        request.on('error', (err) => {
            reject(err);
        });
        request.end();
    });
};

const fetchAppImage = async function fetchAppImage () {
    const aiReleases = 'https://github.com/AppImage/AppImageKit/releases/download/continuous/';
    const files = [
        'runtime-i686',
        'runtime-x86_64',
        'AppRun-i686',
        'AppRun-x86_64'
    ];

    for (let i = 0; i < files.length; i += 1) {
        await fetch(`${aiReleases}${files[i]}`, path.join(__dirname, '..', 'dist'));
    }
};

const wslPath = function wslPath (sourcepath) {
    if (os.platform().includes('win')) {
        return cproc.execSync(`wsl wslpath -a ${sourcepath.replace(/\\/gu, '\\\\')}`, {'encoding': 'utf-8'}).replace(/[\r\n]/gu, '');
    } else {
        return sourcepath;
    }
};

const packageAppImage = function packageAppImage (name, appdir, runtime, dirIcon, compression = null) {
    const absAppdir = wslPath(appdir);
    const absDistdir = wslPath(path.join(__dirname, '..', 'dist'));
    const compArgs = compression
        ? ` -b 1048576 -comp ${compression} -Xdict-size 100%`
        : '';

    cproc.execSync(`bash -c "mkdir ${absAppdir}/usr/bin"`, {'encoding': 'utf-8'});
    cproc.execSync(`bash -c "ln -r -s ${absAppdir}/usr/lib/${pkg.name}/${pkg.name} ${absAppdir}/usr/bin/${pkg.name}"`, {'encoding': 'utf-8'});
    cproc.execSync(`bash -c "ln -r -s ${absAppdir}/usr/lib/assets/${pkg.name}${path.extname(dirIcon)} ${absAppdir}/.DirIcon"`, {'encoding': 'utf-8'});
    cproc.execSync(`bash -c "chmod a+x ${absAppdir}/AppRun"`, {'encoding': 'utf-8'});
    cproc.execSync(`bash -c "mksquashfs ${absAppdir} ${absAppdir}.squashfs -root-owned -noappend${compArgs}"`, {'encoding': 'utf-8'});
    cproc.execSync(`bash -c "rm -f ${absDistdir}/${name}.AppImage"`, {'encoding': 'utf-8'});
    cproc.execSync(`bash -c "cat ${absDistdir}/${runtime} >> ${absDistdir}/${name}.AppImage"`, {'encoding': 'utf-8'});
    cproc.execSync(`bash -c "cat ${absAppdir}.squashfs >> ${absDistdir}/${name}.AppImage"`, {'encoding': 'utf-8'});
    cproc.execSync(`bash -c "chmod a+x ${absDistdir}/${name}.AppImage"`, {'encoding': 'utf-8'});
    cproc.execSync(`bash -c "rm -f ${absAppdir}.squashfs"`, {'encoding': 'utf-8'});
    cproc.execSync(`bash -c "rm -rf ${absAppdir}"`, {'encoding': 'utf-8'});
};

const buildAppImage = function buildAppImage (arch) {
    const buildDef = pkg.build.linux;
    const linuxDist = `${pkg.name}-${pkg.version}-linux-${arch}`;

    if (fs.existsSync(path.join(__dirname, '..', 'dist', linuxDist))) {
        const appDir = path.join(__dirname, '..', 'dist', `${buildDef.displayName}-${arch}.AppDir`);
        const dirIcon = buildDef.diricon
            ? buildDef.diricon
            : buildDef.icon;
        const altArch = arch === 'x86'
            ? 'i686'
            : 'x86_64';

        if (fs.existsSync(path.join(__dirname, '..', 'dist', `${linuxDist}.7z`)) && !pkg.build.targets.includes('7z')) {
            fs.removeSync(path.join(__dirname, '..', 'dist', `${linuxDist}.7z`));
        }

        if (fs.existsSync(path.join(__dirname, '..', 'dist', `${linuxDist}.zip`)) && !pkg.build.targets.includes('zip')) {
            fs.removeSync(path.join(__dirname, '..', 'dist', `${linuxDist}.zip`));
        }

        if (fs.existsSync(appDir)) {
            fs.removeSync(appDir);
        }

        copyRecursiveSync(path.join(__dirname, '..', 'dist', linuxDist), path.join(appDir, 'usr', 'lib', pkg.name));
        fs.writeFileSync(path.join(appDir, `${buildDef.displayName}.desktop`), desktopEntry());
        fs.copySync(path.join(__dirname, '..', buildDef.icon), path.join(appDir, `${buildDef.displayName}${path.extname(buildDef.icon)}`));
        fs.copySync(path.join(__dirname, '..', dirIcon), path.join(appDir, 'usr', 'lib', 'assets', `${pkg.name}${path.extname(dirIcon)}`));
        fs.copySync(path.join(__dirname, '..', 'dist', `AppRun-${altArch}`), path.join(appDir, 'AppRun'));
        packageAppImage(`${buildDef.displayName}-${pkg.version}-${arch}`, appDir, `runtime-${altArch}`, dirIcon, buildDef.compression);
    }
}

const main = async function main () {
    if (pkg.build !== undefined) {
        if (pkg.build.linux !== undefined) {
            // Sample: if (pkg.build.targets.includes('appimage')) {
            await fetchAppImage();

            buildAppImage('x86');
            buildAppImage('x64');
            /*} else {
                console.warn('No appimage target in package.json:build.targets');
            }*/
        } else {
            console.warn('No linux build definition in package.json');
        }
    } else {
        console.warn('No build property in package.json');
    }
};

main();