const buildDmg = function buildDmg () {
    // Example: genisoimage -V progname -D -R -apple -no-pad -o progname.dmg dmgdir
}

const main = async function main () {
    if (pkg.build !== undefined) {
        if (pkg.build.linux !== undefined) {
            if (pkg.build.targets.includes('dmg')) {
                buildDmg();
            } else {
                console.warn('No appimage target in package.json:build.targets');
            }
        } else {
            console.warn('No linux build definition in package.json');
        }
    } else {
        console.warn('No build property in package.json');
    }
};

main();