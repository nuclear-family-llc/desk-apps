const fs = require('fs');
const icnsConvert = require('@fiahfy/icns-convert');
const path = require('path');
const pngToIco = require('png-to-ico');
const png2icons = require('png2icons');
const svg2img = require('svg2img');

const svgToImg = function svgToImg (file, opts = {}) {
    return new Promise((resolve, reject) => {
        svg2img(
            file,
            opts,
            (error, buffer) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(buffer);
                }
            }
        );
    })
};

const main = async function main () {
    const icnsExtra = 1024;
    const pngSizes = [
        72,
        96,
        128,
        144,
        152,
        192,
        256,
        384,
        512,
        icnsExtra
    ];

    console.log('... Generating PNGs from SVG.');

    await svgToImg(path.join(__dirname, '..', 'resources', 'images', 'desk-apps.svg'))
        .then((buffer) => {
            fs.writeFileSync(path.join(__dirname, '..', 'resources', 'images', `desk-apps.png`), buffer);

            console.log('... Generating Windows ICO from generated PNGs.');

            const icoBuffer = png2icons.createICO(buffer, png2icons.RESIZE_BICUBIC2, 0, false, true);

            fs.writeFileSync(path.join(__dirname, '..', 'resources', 'images', `desk-apps.ico`), icoBuffer);
        })
        .catch((err) => {
            console.log(err);
        });

    for (let i = 0; i < pngSizes.length; i += 1) {
        await svgToImg(
            path.join(__dirname, '..', 'resources', 'images', 'desk-apps.svg'),
            {
                width: pngSizes[i],
                height: pngSizes[i]
            }
        )
            .then((buffer) => {
                if (pngSizes[i] === icnsExtra) {
                    console.log('... Generating Apple ICNS from generated PNGs.');

                    const icnsBuffer = png2icons.createICNS(buffer, png2icons.RESIZE_BICUBIC2, 0);

                    fs.writeFileSync(path.join(__dirname, '..', 'resources', 'images', `desk-apps.icns`), icnsBuffer);
                } else {
                    const outputFile = path.join(__dirname, '..', 'resources', 'images', `desk-apps_${pngSizes[i]}x${pngSizes[i]}.png`);

                    fs.writeFileSync(outputFile, buffer);
                }
            })
            .catch((err) => {
                console.log(err);
            });
    }
};

main();