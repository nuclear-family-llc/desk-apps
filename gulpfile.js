/* eslint-disable require-jsdoc, no-sync, max-statements, max-lines-per-function */
const gulp = require('gulp');
const {CLIEngine} = require('eslint');
const jsdoc2md = require('jsdoc-to-markdown');
const depcheck = require('depcheck');
const xmlbuilder = require('xmlbuilder');
const path = require('path');
const fs = require('fs');
const testResults = 'TEST_RESULTS';

const makeTestResultsDir = async function makeTestResultsDir () {
    if (!fs.exists(path.join(__dirname, testResults), (err) => err)) {
        fs.mkdir(path.join(__dirname, testResults), (err) => err);
    }
};

const docs = async function docs () {
    const jsdoc2mdWrite = async function jsdoc2mdWrite (files, filePath, template = null) {
        const jsdoc2mdOptions = {
            files,
            'plugin': 'dmd-readable',
            'no-cache': true
        };

        if (template !== null) {
            jsdoc2mdOptions.template = template;
        }

        await jsdoc2md.render(jsdoc2mdOptions)
            .then((output) => {
                fs.writeFileSync(filePath, output);
            });
    };

    await jsdoc2mdWrite(
        [
            './index.js'
        ],
        path.join(__dirname, 'README.md'),
        fs.readFileSync(path.join(__dirname, 'README.hbs'), 'utf8')
    );
};

const formatDepcheckToJunit = function formatDepcheckToJunit (depcheckOutput) {
    const depcheckJunitDom = {
        'testsuites': {
            'testsuite': []
        }
    };
    const addJunit = ({suiteName, caseName, header, message, dependencyArr, type = 'failure', errors = null}) => {
        const testsuite = {
            '@package': 'org.depcheck',
            '@time': 0,
            '@tests': dependencyArr.length,
            '@errors': errors === null
                ? dependencyArr.length
                : errors,
            '@name': suiteName,
            'testcase': []
        };

        if (Array.isArray(dependencyArr)) {
            dependencyArr.forEach((dependency) => {
                const testcaseName = type === 'success'
                    ? dependency
                    : `org.depcheck.${caseName}`;

                testsuite.testcase.push({
                    '@time': 0,
                    '@name': testcaseName,
                    [type]: {
                        '@message': `'${dependency}' ${message}`,
                        '#cdata': `${header} '${dependency}' ${message}`
                    }
                });
            });
            if (testsuite.testcase.length > 0) {
                depcheckJunitDom.testsuites.testsuite.push(testsuite);
            }
        }
    };

    Object.keys(depcheckOutput).forEach((key) => {
        switch (key) {
        case 'dependencies':
        case 'devDependencies':
            addJunit({
                'suiteName': path.join(__dirname, 'package.json'),
                'caseName': key,
                'header': 'Error in project -',
                'message': 'package is saved to package.json but is never used.',
                'dependencyArr': depcheckOutput[key]
            });
            break;
        case 'missing':
            Object.keys(depcheckOutput[key]).forEach((prop) => {
                addJunit({
                    'suiteName': prop,
                    'caseName': key,
                    'header': 'Error in file -',
                    'message': `file uses ${prop} but package is not saved to package.json.`,
                    'dependencyArr': depcheckOutput[key][prop]
                });
            });
            break;
        case 'using':
            Object.keys(depcheckOutput[key]).forEach((prop) => {
                addJunit({
                    'suiteName': prop,
                    'caseName': key,
                    'header': 'Success in file -',
                    'message': `file uses ${prop} and package is saved to package.json.`,
                    'dependencyArr': depcheckOutput[key][prop],
                    'type': 'success',
                    'errors': 0
                });
            });
            break;
        default:
            break;
        }
    });

    return depcheckJunitDom.testsuites.testsuite.length > 0
        ? xmlbuilder
            .create(depcheckJunitDom)
            .end({'pretty': true})
        : '';
};

const depcheckJunit = async function depcheckJunit () {
    const depcheckOptions = {
        // Ignore dependencies that matches these globs
        'ignoreMatches': [
            'dmd-*',
            'jest',
            'jest-junit',
            'eslint-*',
            'nw*'
        ],
        'ignoreDirs': [
            'certs',
            'dist',
            `${testResults}`
        ]
    };

    await depcheck(path.join(__dirname), depcheckOptions, (depcheckOutput) => {
        const junitXmlString = formatDepcheckToJunit(depcheckOutput);

        if (junitXmlString !== '') {
            fs.writeFileSync(path.join(__dirname, testResults, 'TEST-depcheck.xml'), junitXmlString);
        }
    });
};

const eslintReport = async function eslintReport () {
    const cli = new CLIEngine({
        'useEslintrc': true,
        'ignorePattern': [
            `${testResults}/**/*.js`,
            'dist/**'
        ]
    });
    const htmlReporter = cli.getFormatter('html2');
    const junitReporter = cli.getFormatter('junit');
    const report = cli.executeOnFiles([
        '**/*.js'
    ]);

    await fs.writeFile(path.join(__dirname, testResults, 'eslint-report.html'), htmlReporter(report.results), (err) => err);
    await fs.writeFile(path.join(__dirname, testResults, 'TEST-eslint.xml'), junitReporter(report.results), (err) => err);
};

exports.docs = docs;
exports.depcheckJunit = depcheckJunit;
exports.eslintReport = eslintReport;
exports.test = gulp.series(makeTestResultsDir, gulp.parallel(depcheckJunit, eslintReport));
exports.default = gulp.series(makeTestResultsDir, gulp.parallel(docs, depcheckJunit, eslintReport));
