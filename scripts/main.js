/* eslint-disable max-lines */
/*
 *  Copyright (C) 2019 Nuclear Family LLC
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of
 *  this software and associated documentation files (the "Software"), to deal in
 *  the Software without restriction, including without limitation the rights to
 *  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do
 *  so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 */
const jsAlert = function jsAlert (title, message, cancel = false, ok = true) {
    const buttonCancel = '<button id="alert-cancel" class="button close-alert">Cancel</button>';
    const buttonOk = '<button id="alert-ok" class="button button-primary close-alert">OK</button>';
    const alertModal = `<div class="alert alert-open"><div class="alert-inner"><div class="alert-content"><div class="alert-close-icon"><a href="javascript:void(0)" id="alert-exit" class="close-alert">&times;</i></a></div><div class="alert-content-inner"><h5>${title
        ? message
            ? title
            : 'Alert'
        : 'Alert'}</h5><p>${message
        ? message
        : title
            ? title
            : ''}</p></div><hr class="alert-buttons-seperator"><div class="alert-buttons">${cancel
        ? buttonCancel
        : ''}${ok
        ? buttonOk
        : ''}</div></div></div></div>`;

    const [alertNodeBody] = window.document.getElementsByTagName('body');

    alertNodeBody.onchange = async () => {
        document.querySelector('.alert').classList.add('alert-open');
    };

    alertNodeBody.insertAdjacentHTML('beforeend', alertModal);

    document.querySelectorAll('.close-alert').forEach((alertNodeButton) => {
        alertNodeButton.onclick = (event) => {
            switch (event.target.attributes.id.value) {
            case 'alert-ok':
                if (typeof ok === 'function') {
                    ok();
                }
                break;
            case 'alert-cancel':
                if (typeof cancel === 'function') {
                    cancel();
                }
                break;
            default:
                break;
            }
            document.querySelector('.alert').classList.remove('alert-open');
            window.document.getElementById('body').removeChild(document.querySelector('.alert'));
            alertNodeBody.onchange = null;
        };
    });

    document.querySelector('.alert-inner').onclick = () => {
        document.querySelector('.alert').classList.remove('alert-open');
        window.document.getElementById('body').removeChild(document.querySelector('.alert'));
    };

    document.querySelector('.alert-content').onclick = (event) => {
        event.stopPropagation();
    };
};

const lastPassId = 'hdokiejnpimakedhajhdlcegeplioahd';

const isNWJS = function isNWJS () {
    try {
        return typeof require('nw.gui') !== 'undefined';
    } catch (err) {
        return false;
    }
};

const isNotEdgeFileMode = function isNotEdgeFileMode (doAlert) {
    // Internet Explorer 6-11
    const isIE = /*@cc_on!@*/false || !!document.documentMode; // eslint-disable-line no-inline-comments, spaced-comment, no-implicit-coercion
    // Edge 20+
    const isEdge = !isIE && !!window.StyleMedia; // eslint-disable-line no-implicit-coercion

    if (isEdge && window.location.protocol === 'file:') {
        if (doAlert) {
            jsAlert('You are using Edge in file protocol mode; adding, editing, and removing apps is not supported in this case. Please use Chrome, Firefox, or serve this page from http or https if you wish to use Edge.');
        }

        return false;
    }

    return true;
};

const openAddWebAppDialog = async function openAddWebAppDialog () {
    if (isNotEdgeFileMode(true)) {
        window.document.getElementById('web-app-add-modal').style.display = 'block';
    }
};

const closeAddWebAppDialog = async function closeAddWebAppDialog () {
    window.document.getElementById('web-app-form').reset();
    window.document.getElementById('web-app-add-modal').style.display = 'none';
};

const buildWebAppHtml = function buildWebAppHtml (webAppArr) {
    const appDefaults = {
        'width': 'screen.availWidth',
        'height': 'screen.availHeight',
        'fullScreen': true,
        'limit': false,
        'icon': '../resources/images/desk-apps.svg'
    };
    const appLinks = [];
    const sorter = function sorter (appA, appB) {

        return appA.name.toLowerCase() < appB.name.toLowerCase()
            ? -1
            : appA.name.toLowerCase() > appB.name.toLowerCase()
                ? 1
                : 0;
    };

    webAppArr.sort(sorter);

    if (webAppArr.length > 0) {
        webAppArr.forEach((webApp) => {
            const appLinkID = webApp.name.replace(/ /gu, '-').toLowerCase();
            const width = webApp.window.width
                ? webApp.window.width
                : appDefaults.width;
            const height = webApp.window.height
                ? webApp.window.height
                : appDefaults.height;
            const fullScreen = webApp.window.fullScreen
                ? webApp.window.fullScreen
                : appDefaults.fullScreen;
            const limit = webApp.window.limit
                ? webApp.window.limit
                : appDefaults.limit;
            const appIcon = webApp.icon
                ? webApp.icon
                : appDefaults.icon;

            appLinks.push(`<div class="web-app"><div class="web-app-shortcut" app="${webApp.name}">
    <a class="web-app-shortcut" id="${appLinkID}" app="${webApp.name}" href="javascript:void(0)" onclick="openWebApp('${webApp.url}', '${webApp.name}', ${width}, ${height}, '${appIcon}', ${fullScreen}, ${limit})">
        <img class="web-app-shortcut" app="${webApp.name}" src="${appIcon}" alt="Desk App Icon" width="60" height="60" />
        <p class="web-app-shortcut" app="${webApp.name}">${webApp.name}</p>
    </a></div></div>`);
        });
        appLinks.push('<div class="web-app"><button class="plus-button" onclick="openAddWebAppDialog()" id="plus-button" style="">+</button></div>');
    } else {
        appLinks.push('<div class="web-app"><button onclick="openAddWebAppDialog()" id="plus-button" style="background: #0061ff; color: white;">Add Your First Web App</button></div>');
    }

    return appLinks.join('\n');
};

const buildLauncherHtml = function buildLauncherHtml (webApps) {

    return `<div id="web-apps-title">
    <h5 class="modal-title">Desk Apps</h5>
    <p class="subtitle">Open web apps like a desktop application${isNWJS()
        ? ''
        : ' from your browser'}</p>
</div>
<div id="web-app-launcher" role="dialog">${buildWebAppHtml(webApps)}</div>
<div id="web-app-footer">
    <div class="web-app-add"><button onclick="openAddWebAppDialog()" id="add-button">Add Web App</button></div>
    <div class="web-app-add"><button onclick="exportApps()" id="export-button">Export Apps</button></div>
    <div class="web-app-add"><button onclick="importApps()" id="import-button">Import Apps</button></div>
    <div id="web-app-lastpass"><button onclick="gotoLastPass()" id="install-button">Install LastPass</button></div>
</div>
<div id="web-app-add-modal" class="modal">
    <div class="modal-content"><div class="modal-header"><div class="form-header"><h4>Add Web App</h4></div></div><div class="modal-body"><form id="web-app-form">
                <input type="hidden" name="appid" value="">
                App Name (*):<br>
                <input type="text" name="appname" value="" required>
                <br>
                App URL (*):<br>
                <input type="url" name="appurl" value="" required>
                <br>
                <input type="checkbox" name="appfullscreen" value="true" checked> Maximize app to full screen<br>
                <input type="checkbox" name="applimit" value="true"> Limit app to one window<br>
                App window width:<br>
                <input type="number" name="appwidth" min="0" max="1600">
                <br>
                App window height:<br>
                <input type="number" name="appheight" min="0" max="1600">
                <br>
                App Icon URL:<br>
                <input type="url" name="appicon" value="">
                <br>
                <input type="submit" name="appsubmit" onclick="addWebApp()" value="Submit">
                <button type="button" onclick="closeAddWebAppDialog()">Cancel</button>
                <br>
                <p class="form-footer">(*) Required field.</p>
            </form></div></div></div>`;
};

const getLauncherApps = async function getLauncherApps () {
    const webApps = await fetch('./../config/desk-apps.json')
        .then((response) => response.json())
        .catch(() => []);

    if (isNotEdgeFileMode(false)) {
        const localWebApps = window.localStorage.getItem('WebApps')
            ? JSON.parse(window.localStorage.getItem('WebApps'))
            : JSON.parse('[]');

        localWebApps.forEach((localWebApp) => {
            const webAppIndex = webApps.findIndex((webApp) => webApp.name === localWebApp.name);

            if (webAppIndex >= 0) {
                webApps.splice(webAppIndex, 1);
            }
            webApps.push(localWebApp);
        });
    }

    return webApps;
};

const webAppLauncher = async function webAppLauncher (doc, final = false) {
    const webApps = await getLauncherApps()
        .catch(() => []);

    doc.body.innerHTML = buildLauncherHtml(webApps);

    if (isNWJS()) {
        const fs = require('fs');
        const path = require('path');

        if (!fs.existsSync(path.join(nw.App.dataPath, 'Extensions', lastPassId))) {
            document.getElementById('web-app-lastpass').style.display = 'block';
        }
        window.document.title = `${window.document.title} - LAUNCHER`;
    }

    if (final) {
        final();
    }
};

const gotoLastPass = function gotoLastPass () {
    const lastPassExtUrl = `https://chrome.google.com/webstore/detail/${lastPassId}`;

    window.open(lastPassExtUrl, 'chrome-webstore-lastpass').focus();
};

const openWebApp = function openWebApp (url, name, xCoord, yCoord, icon, fullScreen, limit) {
    const windowFeatures = 'directories=no,titlebar=yes,toolbar=no,location=no,status=yes,menubar=no,scrollbars=yes,noopener=no,width=600,height=600';

    const targetName = limit
        ? `_${name.replace(/ /gu, '_').toLowerCase()}`
        : '_blank';

    const newWindow = () => {
        const windowOpen = window.open(url, targetName, windowFeatures);

        if (fullScreen) {
            windowOpen.moveTo(0, 0);
        }

        windowOpen.resizeTo(xCoord, yCoord);

        windowOpen.document.title = name;

        return windowOpen;
    };

    const getHref = (windowObj) => {
        try {
            return windowObj.location.href;
        } catch (ex) {
            return false;
        }
    };

    const windowSingleton = () => {
        let getWindow = window.open('', targetName, windowFeatures);

        if (getHref(getWindow) === 'about:blank' || getWindow.closed) {
            getWindow = newWindow();
        }

        return getWindow;
    };

    const newWebApp = limit
        ? windowSingleton()
        : newWindow();

    newWebApp.focus();
};

const exportApps = async function exportApps () {
    const fileName = 'desk-apps.json';

    jsAlert('Export Apps', `This will initiate a download of apps to a '${fileName}' file.`, true, async () => {
        const webApps = await fetch(`./../config/${fileName}`)
            .then((response) => response.json())
            .catch(() => []);
        const localWebApps = window.localStorage.getItem('WebApps')
            ? JSON.parse(window.localStorage.getItem('WebApps'))
            : JSON.parse('[]');

        const element = document.createElement('a');

        element.setAttribute('href', `data:text/plain;charset=utf-8,${encodeURIComponent(JSON.stringify(webApps.concat(localWebApps), null, 2))}`);
        element.setAttribute('download', fileName);

        element.style.display = 'none';
        document.body.appendChild(element);

        element.click();

        document.body.removeChild(element);
    });
};

const checkWebAppConfigSchema = function checkWebAppConfigSchema (testObject) {
    if (Array.isArray(testObject)) {
        /* eslint-disable-next-line max-statements */
        testObject.forEach((importedApp) => {
            if (!importedApp.name || importedApp.name === '') {
                throw Error('name');
            }
            if (!importedApp.url || importedApp.url === '') {
                throw Error('url');
            }
            if (!importedApp.window) {
                throw Error('window');
            }
            if (importedApp.window.width !== undefined) {
                if (importedApp.window.width <= 0) {
                    throw Error('window.width');
                }
            }
            if (importedApp.window.height !== undefined) {
                if (importedApp.window.height <= 0) {
                    throw Error('window.height');
                }
            }
            if (importedApp.window.fullScreen !== undefined) {
                if (typeof importedApp.window.fullScreen !== 'boolean') {
                    throw Error('window.fullScreen');
                }
            }
            if (importedApp.window.limit !== undefined) {
                if (typeof importedApp.window.limit !== 'boolean') {
                    throw Error('window.limit');
                }
            }
            if (importedApp.icon !== undefined) {
                if (typeof importedApp.icon !== 'string') {
                    throw Error('icon');
                }
            }
        });
    } else {
        throw TypeError('The configuration is not a valid JSON array.');
    }
};

const importErrorHandler = function importErrorHandler (err) {
    if (err instanceof SyntaxError) {
        jsAlert('The file you have provided is not valid JSON.');
    } else if (err instanceof TypeError) {
        jsAlert('The configuration is not a valid JSON array.');
    } else if (err instanceof Error) {
        switch (err.message) {
        case 'name':
        case 'url':
            jsAlert(`One or more apps have a missing or invalid ${err.message} property.`);
            break;
        case 'window':
            jsAlert('One or more apps have a missing window object.');
            break;
        case 'window.width':
        case 'window.height':
        case 'window.fullScreen':
        case 'window.limit':
        case 'icon':
            jsAlert(`One or more apps have an invalid ${err.message} property.`);
            break;
        default:
            jsAlert(err.message);
        }
    }
};

const importApps = function importApps () {
    const element = document.createElement('input');

    element.setAttribute('type', 'file');
    element.setAttribute('accept', 'application/json');
    element.setAttribute('id', 'import-file');

    element.style.display = 'none';

    document.body.appendChild(element);

    element.onchange = () => {
        jsAlert(
            'Import Apps',
            `This will initiate an import of apps from '${element.files[0].name}'. It will overwrite any web apps you have added.`,
            async () => {
                document.body.removeChild(element);
            },
            async () => {
                const fileReader = new FileReader();

                fileReader.onload = function onload () {
                    try {
                        const importedApps = JSON.parse(fileReader.result);

                        checkWebAppConfigSchema(importedApps);
                        window.localStorage.setItem('WebApps', JSON.stringify(importedApps));
                        fileReader.readAsText(element.files[0], 'utf-8');
                    } catch (err) {
                        importErrorHandler(err);
                    } finally {
                        document.body.removeChild(element);
                        webAppLauncher(document);
                    }
                };
            }
        );
    };

    element.click();
};

const addWebApp = function addWebApp () { // eslint-disable-line max-statements
    if (isNotEdgeFileMode(true)) {
        const appnameEl = window.document.querySelector('input[name="appname"]');
        const appurlEl = window.document.querySelector('input[name="appurl"]');
        const appiconEl = window.document.querySelector('input[name="appicon"]');

        if (appnameEl.checkValidity() && appurlEl.checkValidity() && appiconEl.checkValidity()) {
            const localWebApps = window.localStorage.getItem('WebApps')
                ? JSON.parse(window.localStorage.getItem('WebApps'))
                : JSON.parse('[]');
            const appId = window.document.querySelector('input[name="appid"]').value;
            const localWebAppIndex = localWebApps.findIndex((webApp) => webApp.name === appId);

            if (localWebAppIndex >= 0) {
                localWebApps.splice(localWebAppIndex, 1);
            }

            const webApp = {
                'name': appnameEl.value,
                'url': appurlEl.value,
                'window': {}
            };

            const baseTen = 10;

            if (parseInt(window.document.querySelector('input[name="appwidth"]').value, baseTen) > 0) {
                webApp.window.width = parseInt(window.document.querySelector('input[name="appwidth"]').value, baseTen);
            }
            if (parseInt(window.document.querySelector('input[name="appheight"]').value, baseTen) > 0) {
                webApp.window.height = parseInt(window.document.querySelector('input[name="appheight"]').value, baseTen);
            }
            if (!window.document.querySelector('input[name="appfullscreen"]').checked) {
                webApp.window.fullScreen = window.document.querySelector('input[name="appfullscreen"]').checked;
            }
            if (window.document.querySelector('input[name="applimit"]').checked) {
                webApp.window.limit = window.document.querySelector('input[name="applimit"]').checked;
            }
            if (appiconEl.value !== '') {
                webApp.icon = appiconEl.value;
            }

            localWebApps.push(webApp);
            window.localStorage.setItem('WebApps', JSON.stringify(localWebApps));

            window.document.getElementById('web-app-form').reset();
            window.document.getElementById('web-app-add-modal').style.display = 'none';
            webAppLauncher(document);
        } else {
            jsAlert('App Name and App URL are required fields and must be valid data.\nApp Icon URL may be blank, but otherwise must be valid.');
        }
    }
};

const editWebApp = async function editWebApp (localWebApp) {
    window.document.querySelector('input[name="appid"]').value = localWebApp.name;
    window.document.querySelector('input[name="appname"]').value = localWebApp.name;
    window.document.querySelector('input[name="appurl"]').value = localWebApp.url;
    window.document.querySelector('input[name="appwidth"]').value = localWebApp.window.width || 0;
    window.document.querySelector('input[name="appheight"]').value = localWebApp.window.height || 0;
    window.document.querySelector('input[name="appfullscreen"]').checked = typeof localWebApp.window.fullScreen === 'boolean'
        ? localWebApp.window.fullScreen
        : true;
    window.document.querySelector('input[name="applimit"]').checked = typeof localWebApp.window.limit === 'boolean'
        ? localWebApp.window.limit
        : false;
    window.document.querySelector('input[name="appicon"]').value = localWebApp.icon || '';
    openAddWebAppDialog();
};

const updateOrRemoveWebApp = async function updateOrRemoveWebApp (appName, update) {
    if (isNotEdgeFileMode(true)) {
        const webApps = await fetch('./../config/desk-apps.json')
            .then((response) => response.json())
            .catch(() => []);
        const localWebApps = window.localStorage.getItem('WebApps')
            ? JSON.parse(window.localStorage.getItem('WebApps'))
            : JSON.parse('[]');

        if (localWebApps.findIndex((webApp) => webApp.name === appName) >= 0 && !update) {
            jsAlert('Remove Web App', 'Are you sure that you want to remove this web app?', true, () => {
                localWebApps.splice(localWebApps.findIndex((webApp) => webApp.name === appName), 1);
                window.localStorage.setItem('WebApps', JSON.stringify(localWebApps));
                webAppLauncher(document);
            });
        } else if (localWebApps.findIndex((webApp) => webApp.name === appName) >= 0 && update) {
            editWebApp(localWebApps[localWebApps.findIndex((webApp) => webApp.name === appName)]);
        } else if (webApps.findIndex((webApp) => webApp.name === appName) >= 0 && !update) {
            jsAlert(`App '${appName}' is a built-in app; it cannot be removed. Please contact your web administrator to remove the built-in apps.`);
        } else if (webApps.findIndex((webApp) => webApp.name === appName) >= 0 && update) {
            jsAlert('Edit Built-in App', `App '${appName}' is a built-in app; are you sure that you want to edit this web app?`, true, async () => {
                editWebApp(webApps[webApps.findIndex((webApp) => webApp.name === appName)]);
            });
        }
    }
};

const loadAppLauncher = async function loadAppLauncher () {
    webAppLauncher(document, async () => {
        window.document.querySelector('input[name="appsubmit"]').onclick = () => {
            addWebApp();
        };
        window.document.getElementById('add-button').onclick = () => {
            openAddWebAppDialog();
        };
        window.document.getElementById('export-button').onclick = () => {
            exportApps();
        };
        window.document.getElementById('import-button').onclick = () => {
            importApps();
        };
        window.document.getElementById('install-button').onclick = () => {
            gotoLastPass();
        };
    });
};

window.onload = loadAppLauncher;

window.openWebApp = openWebApp;

window.onclick = function onclick (event) {
    switch (event.target) {
    case window.document.getElementById('web-app-add-modal'):
        closeAddWebAppDialog();
        break;
    case window.document.querySelector('input[name="appsubmit"]'):
        event.preventDefault();
        break;
    case window.document.getElementById('web-app-edit'):
        updateOrRemoveWebApp(event.target.parentNode.getAttribute('app'), true);
        break;
    case window.document.getElementById('web-app-remove'):
        updateOrRemoveWebApp(event.target.parentNode.getAttribute('app'), false);
        break;
    default:
        break;
    }
    if (event.target.id !== 'contextMenu' && window.document.getElementById('contextMenu')) {
        window.document.getElementById('body').removeChild(window.document.getElementById('contextMenu'));
    }
};

window.oncontextmenu = function oncontextmenu (event) {
    if (event.target.className === 'web-app-shortcut') {
        event.preventDefault();
        const ctxMenu = `<menu id="contextMenu" app="${event.target.getAttribute('app')}" style="left: ${event.pageX}px; top: ${event.pageY}px">
    &#9776;
    <menu id="web-app-edit" title="Edit">Edit</menu>
    <menu id="web-app-remove" title="Remove">Remove</menu>
</menu>`;

        window.document.getElementById('body').insertAdjacentHTML('beforeend', ctxMenu);
    }
};
